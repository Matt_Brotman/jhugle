//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - jyin14@jhu.edu

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

/**
 * Ordered maps implemented as (basic) binary search trees.
 *
 * @param <K> Type for keys.
 * @param <V> Type for values.
 */
public class TreapMap<K extends Comparable<? super K>, V>
    implements OrderedMap<K, V> {

    // Inner node class
    private class Node {
        Node left;
        Node right;
        K key;
        V value;
        int priority;

        Node(K k, V v, int p) {
            this.priority = p;
            this.key = k;
            this.value = v;
        }

        public String toString() {
            return "Node<key: " + this.key
                + "; value: " + this.value
                + ">";
        }
    }

    private Node root;
    private int size;
    private StringBuilder stringBuilder;
    private Random rand;

    /**
     * Default TreapMap constructor, for our Random generator.
     */
    public TreapMap() {
        this.rand = new Random();
    }

    @Override
    public int size() {
        return this.size;
    }

    // Return node for given key.
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        Node n = this.root;
        while (n != null) {
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) != null;
    }

    // Return node for given key
    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    @Override
    public void put(K k, V v) {
        Node n = this.findForSure(k);
        n.value = v;
    }

    @Override
    public V get(K k) {
        Node n = this.findForSure(k);
        return n.value;
    }

    private Node rotateFromLeft(Node n) {
        Node l = n.left;
        n.left = l.right;
        l.right = n;
        return l;
    }

    private Node rotateFromRight(Node n) {
        Node r = n.right;
        n.right = r.left;
        r.left = n;
        return r;
    }

    // Insert given key and value into subtree rooted at given node;
    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v, this.rand.nextInt());
        }

        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.insert(n.left, k, v);
            if (n.left.priority < n.priority) {
                n = this.rotateFromLeft(n);
            }
        } else if (cmp > 0) {
            n.right = this.insert(n.right, k, v);
            if (n.right.priority < n.priority) {
                n = this.rotateFromRight(n);
            }
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }

        return n;
    }

    @Override
    public void insert(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v);
        this.size++;
    }

    // Return node with maximum key in subtree rooted at given node.
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }

    // Remove given node and return the remaining tree.
    // Swap down until the n is a leaf
    private Node remove(Node n) {
        // 0 and 1 child
        if (n.left == null && n.right == null) {
            return null;
        } else if (n.right == null
                   || (n.left != null && n.right.priority > n.left.priority)) {
            n = this.rotateFromLeft(n);
            n.right = this.remove(n.right);
        } else {
            n = this.rotateFromRight(n);
            n.left = this.remove(n.left);
        }

        return n;
    }

    // Remove node with given key from subtree rooted at given node
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }

        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.remove(n.left, k);
        } else if (cmp > 0) {
            n.right = this.remove(n.right, k);
        } else {
            n = this.remove(n);
        }

        return n;
    }

    @Override
    public V remove(K k) {
        Node r = this.findForSure(k);
        this.root = this.remove(this.root, k);
        this.size--;
        return r.value;
    }

    // Recursively add keys from subtree rooted at given node into the
    // given list in order.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }

    // Recursively append string representations of keys and values from
    // subtree rooted at given node in order.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of the last ", "
            // toStringHelper put in; easier to correct this after the
            // fact than to avoid making the mistake in the first place.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");

        return this.stringBuilder.toString();
    }
}

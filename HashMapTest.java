//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - jyin14@jhu.edu

/**
 * Hash map implementation specific test.
 */
public class HashMapTest extends MapTestBase {
    @Override
    protected Map<String, Integer> createMap() {
        return new HashMap<String, Integer>();
    }
}

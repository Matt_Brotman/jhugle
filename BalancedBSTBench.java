//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - jyin14@jhu.edu

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Performance Benchmarks for AVL Trees and Treaps
 */
public final class BalancedBSTBench {
    private static final int SIZE = 300;
    private static final Random RAND = new Random();
    private static ArrayList<String> rands;
    private static ArrayList<String> ords;

    private BalancedBSTBench() {}

    private static void setupLists() {
        rands = new ArrayList<String>();
        ords = new ArrayList<String>();
        for (int i = 0; i < SIZE; i++) {
            ords.add(Integer.toString(i));
            rands.add(Integer.toString(i));
        }
        Collections.sort(ords);
        Collections.shuffle(rands);
    }

    // Insert a number of "consecutive" strings into the given map.
    private static void insertOrdered(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(ords.get(i), i);
        }
    }

    // Insert a number of "random" strings into the given map.
    private static void insertRandom(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(rands.get(i), i);
        }
    }

    // Remove a number of ordered strings from the given map.
    private static void removeAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.remove(Integer.toString(i));
        }
    }

    private static void putAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put(Integer.toString(i), i + 1);
        }
    }

    private static void getAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            Integer num = m.get(Integer.toString(i));
        }
    }

    // Lookup a number of strings in the given map.
    private static void lookupAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = m.has(Integer.toString(i));
        }
    }

    private static void mixAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            Integer num = m.remove(Integer.toString(i));
            m.insert(Integer.toString(i), num);
        }
    }

    
    // Now the benchmarks we actually want to run.
    @Bench
    public static void insertOrderedAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            b.start();
            insertOrdered(m);
        }
    }

    @Bench
    public static void insertRandomAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            b.start();
            insertRandom(m);
        }
    }

    @Bench
    public static void removeOrderedAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            removeAll(m);
        }
    }

    @Bench
    public static void removeRandomAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            removeAll(m);
        }
    }

    @Bench
    public static void putOrderedAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            putAll(m);
        }
    }

    @Bench
    public static void putRandomAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            putAll(m);
        }
    }

    @Bench
    public static void getOrderedAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            getAll(m);
        }
    }

    @Bench
    public static void getRandomAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            getAll(m);
        }
    }

    @Bench
    public static void lookupOrderedAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            lookupAll(m);
        }
    }

    @Bench
    public static void lookupRandomAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            lookupAll(m);
        }
    }

    @Bench
    public static void mixedOrderedAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            mixAll(m);
        }
    }

    @Bench
    public static void mixedRandomAvlMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            mixAll(m);
        }
    }
}

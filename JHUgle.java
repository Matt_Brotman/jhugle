//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - Jyin14@jhu.edu

import java.io.IOException;
import java.util.Scanner;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayDeque;
import java.util.Deque;

import java.nio.file.Files;
import java.nio.file.Paths;

//for timing with java hashmap against our own
//import java.util.Map;
//import java.util.HashMap;

/**
 * JHU version of google, works with any map but best with our HashMap.
 */
public final class JHUgle {

    //Checkstyle
    private JHUgle() {}

    //Fill our map
    private static void fillData(Map<String, Set<String>> data, String file) {
        List<String> allLines;
        try {
            allLines = Files.readAllLines(Paths.get(file));

            for (int i = 0; i < allLines.size(); i += 2) {
                String[] words = allLines.get(i + 1).split("\\s+");
                String url = allLines.get(i);

                for (int j = 0; j < words.length; j++) {
                    String word = words[j];

                    Set<String> associatedUrls;
                    if (data.has(word)) {
                        associatedUrls = data.get(word);
                        associatedUrls.add(url);
                        data.put(word, associatedUrls);
                    } else {
                        associatedUrls = new TreeSet<>();
                        associatedUrls.add(url);
                        data.insert(word, associatedUrls);
                    }

                    //below is the code for java's hashmap for xtime
                    //kept in so we could xtime efficiently
                    /*
                    Set<String> associatedUrls = data.get(word);
                    if (associatedUrls == null) {
                        associatedUrls = new TreeSet<String>();
                    }
                    associatedUrls.add(url);
                    data.put(word, associatedUrls);
                    */
                }
            }
        } catch (IOException e) {
            System.err.println("Could not read file.");
            System.exit(1);
        }
    }

    private static void evaluateSets(Deque<Set<String>> storedSets,
                                      Map<String, Set<String>> data,
                                      boolean isIntersection) {
        if (storedSets.size() >= 2) {
            Set<String> firstSet = storedSets.pop();
            Set<String> secondSet = storedSets.pop();
            if (isIntersection) {
                secondSet.retainAll(firstSet);
            } else {
                secondSet.addAll(firstSet);
            }
            storedSets.push(secondSet);
        } else {
            System.err.println("Unable to perform operation.");
        }
    }

    //Does stuff with user's input
    private static boolean performActionsWithInput(String input,
                                                Deque<Set<String>> storedSets,
                                                Map<String, Set<String>> data) {
        if ("!".equals(input)) {
            return true;
        } else if ("?".equals(input)) {
            if (!storedSets.isEmpty()) {
                Set<String> urls = storedSets.peek();
                for (String url : urls) {
                    System.out.println(url);
                }
            }
        } else if ("&&".equals(input)) {
            evaluateSets(storedSets, data, true);
        } else if ("||".equals(input)) {
            evaluateSets(storedSets, data, false);
        } else {
            try {
                storedSets.push(data.get(input));
            } catch (IllegalArgumentException | NullPointerException e) {
                System.err.println("No results for "
                                   + input);
            }
        }
        return false;
    }

    //Allows user to input keywords and get back the associated urls
    private static void searchWithData(Map<String, Set<String>> data) {
        Scanner scanner = new Scanner(System.in);
        boolean shouldEnd = false;
        Deque<Set<String>> inputDeque = new ArrayDeque<Set<String>>();
        while (!shouldEnd && scanner.hasNext()) {
            String next = scanner.next();
            shouldEnd = performActionsWithInput(next, inputDeque, data);
        }
    }

    /**
     * Main Method.
     * @param args Command Line argument (file of words to url's).
     * @throws IOException in case of loss of input pressure.
     */
    public static void main(String[] args) throws IOException {

        Map<String, Set<String>> data = new HashMap<>(); //map
        fillData(data, args[0]);
        System.err.println("Index Created");

        searchWithData(data);
    }
}

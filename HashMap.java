//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - jyin14@jhu.edu

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Our custom HashMap implementation.
 * Uses a linear probing technique for finding keys after hashing
 * We use a standard java array, linear probing, and tombstones upon deleting
 * in order to account for the primary collisioning that is bound to occur
 * We also started with an initial array size of 8
 * and a load factor of 0.5 (see README)
 *
 * @author Jason Yin Matt Brotman
 * @param <K> Key
 * @param <V> Value
 */
public class HashMap<K, V> implements Map<K, V> {

    private class Entry<K, V> {
        K key;
        V value;
        boolean tombstone;

        Entry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public void setTombstone() {
            this.tombstone = true;
        }

        public boolean isTombstone() {
            return this.tombstone;
        }
    }

    private Entry<K, V>[] data;
    private int size;
    private int length;

    private int lastViewedIndex;
    private K lastViewedKey;

    /**
     * Constructor for HashMap.
     */
    public HashMap() {
        this.size = 0;
        this.length = 8;
        this.data = (Entry<K, V>[]) Array.newInstance(Entry.class, this.length);
    }

    private int fasterMod(int val) {
        return val & (this.length - 1);
    }

    private int hash(K k) {
        return this.fasterMod(k.hashCode());
    }

    private int findIndex(K k) {
        int index = this.hash(k);

        Entry<K, V> entry = this.data[index];
        while (entry != null) {
            if (k.equals(entry.key)) {
                return index;
            } else {
                index++;
                index = this.fasterMod(index);
                entry = this.data[index];
            }
        }
        // if array was completely traversed then
        // element does not exist
        return -1;
    }

    private Entry<K, V> find(K k) {

        // if array was completely traversed then
        // element does not exist
        int index = this.findIndex(k);
        if (index < 0) {
            return null;
        } else {
            return this.data[index];
        }
    }

    // find the next empty index or throw an exception if the key exists
    private int insertionHelper(K k) {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int index = this.hash(k);
        Entry<K, V> entry = this.data[index];
        while (entry != null) {
            if (k.equals(entry.key)
                && !entry.isTombstone()) {
                throw new IllegalArgumentException();
            }
            index++;
            index = this.fasterMod(index);
            entry = this.data[index];
        }
        return index;
    }

    private void checkLoadFactor() {
        double loadFactor = (double) this.size / this.length;
        if (loadFactor > 0.5) {
            // rehash everything
            Entry<K, V>[] temp = this.data;
            this.length *= 2;
            this.size = 0;
            this.data = (Entry<K, V>[])
                    Array.newInstance(Entry.class, this.length);
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null && !temp[i].isTombstone()) {
                    Entry<K, V> entry = temp[i];
                    this.insert(entry.key, entry.value);
                }
            }
        }
    }

    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        int index = this.insertionHelper(k);
        this.data[index] = new Entry<K, V>(k, v);
        this.size++;
        this.checkLoadFactor();
    }

    @Override
    public V remove(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int index = this.findIndex(k);
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        Entry<K, V> entry = this.data[index];
        entry.setTombstone();
        entry.key = null;
        this.size--;
        return entry.value;
    }

    @Override
    public void put(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }

        if (k.equals(this.lastViewedKey)) {
            this.data[this.lastViewedIndex] = new Entry<K, V>(k, v);
        } else {
            int index = this.findIndex(k);
            if (index < 0) {
                throw new IllegalArgumentException();
            }
            this.data[index] = new Entry<K, V>(k, v);
        }
    }

    @Override
    public V get(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        Entry<K, V> entry = this.find(k);
        if (entry == null) {
            throw new IllegalArgumentException();
        }
        return entry.value;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }

        int index = this.findIndex(k);
        this.lastViewedIndex = index;
        this.lastViewedKey = k;
        return index >= 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<K> iterator() {
        ArrayList<K> keys = new ArrayList<>();
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i] != null && !this.data[i].isTombstone()) {
                keys.add(this.data[i].key);
            }
        }
        return keys.iterator();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("{");
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i] != null && !this.data[i].isTombstone()) {
                s.append(this.data[i].key);
                s.append(": ");
                s.append(this.data[i].value);
                s.append(", ");
            }
        }
        // backspace the ", "
        s.setLength(s.length() - 2);
        s.append("}");
        return s.toString();
    }

}

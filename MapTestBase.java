//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - jyin14@jhu.edu

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for all implementations of the map interface.
 */
public abstract class MapTestBase {
    private Map<String, Integer> m;

    protected abstract Map<String, Integer> createMap();

    @Before
    public void setupMapTests() {
        m = this.createMap();
    }

    @Test
    public void newMapEmpty() {
        assertEquals(0, m.size());
    }

    @Test
    public void insertWorks() {
        for (int i = 1; i < 20; i++) {
            m.insert(Integer.toString(i), i);
            assertEquals(i, m.size());
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void insertThrowsCorrectException() {
        m.insert("first", 1);
        m.insert("first", 2);
    }

    @Test
    public void hasWorks() {
        for (int i = 1; i < 20; i++) {
            m.insert(Integer.toString(i), i);
            assertFalse(m.has(Integer.toString(i+1)));
        }
        for (int i = 1; i < 20; i++) {
            assertTrue(m.has(Integer.toString(i)));
        }
    }

    @Test
    public void hasOnNull() {
        for (int i = 1; i < 20; i++) {
            m.insert(Integer.toString(i), i);
            assertFalse(m.has(Integer.toString(i+1)));
        }
        boolean test = m.has(null);
        assertFalse(test);
    }

    @Test
    public void getWorks() {
        for (int i = 1; i < 20; i++) {
            m.insert(Integer.toString(i), i);
        }
        for (int i = 1; i < 20; i++) {
            int n = m.get(Integer.toString(i));
            assertEquals(n, i);
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void getThrowsCorrectException() {
        m.insert("first", 1);
        m.get("f");
    }

    @Test
    public void putWorks() {
        for (int i = 1; i < 20; i++) {
            m.insert(Integer.toString(i), i);
        }
        for (int i = 1; i < 20; i++) {
            m.put(Integer.toString(i), i + 1);
        }
        for (int i = 1; i < 20; i++) {
            int n = m.get(Integer.toString(i));
            assertEquals(i + 1, n);
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void putThrowsCorrectException() {
        m.insert("first", 1);
        m.put("f", 2);
    }

    @Test
    public void removeWorks() {
        for (int i = 1; i < 31; i++) {
            m.insert(Integer.toString(i), i);
        }
        for (int i = 1; i < 31; i++) {
            if (i % 6 == 0) {
                m.remove(Integer.toString(i));
            }
        }
        for (int i = 1; i < 31; i++) {
            if (i % 6 == 0) {
                assertFalse(m.has(Integer.toString(i)));
            } else {
                assertTrue(m.has(Integer.toString(i)));
                int n = m.get(Integer.toString(i));
                assertEquals(i , n);
            }
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void removeThrowsCorrectException() {
        m.insert("first", 1);
        m.remove("first");
        m.remove("first");
    }

    @Test
    public void insertAfterRemoveWorks() {
        for (int i = 1; i < 31; i++) {
            m.insert(Integer.toString(i), i);
        }
        for (int i = 1; i < 31; i+=3) {
            m.remove(Integer.toString(i));
        }
        for (int i = 1; i < 31; i++) {
            if (!m.has(Integer.toString(i))) {
                m.insert(Integer.toString(i), i);
            }
        }
    }
}

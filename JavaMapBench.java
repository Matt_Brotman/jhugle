//Matt Brotman - mbrotma1@jhu.edu
//Jason Yin - jyin14@jhu.edu

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Map;
import java.util.HashMap;

/**
 * Performance Benchmarks for Java's Map
 */
public final class JavaMapBench {
    private static final int SIZE = 300;
    private static final Random RAND = new Random();
    private static ArrayList<String> rands;
    private static ArrayList<String> ords;

    private JavaMapBench() {}

    private static void setupLists() {
        rands = new ArrayList<String>();
        ords = new ArrayList<String>();
        for (int i = 0; i < SIZE; i++) {
            ords.add(Integer.toString(i));
            rands.add(Integer.toString(i));
        }
        Collections.sort(ords);
        Collections.shuffle(rands);
    }

    // Insert a number of "consecutive" strings into the given map.
    private static void insertOrdered(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put(ords.get(i), i);
        }
    }

    // Insert a number of "random" strings into the given map.
    private static void insertRandom(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put(rands.get(i), i);
        }
    }

    // Remove a number of ordered strings from the given map.
    private static void removeAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.remove(Integer.toString(i));
        }
    }

    private static void putAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put(Integer.toString(i), i + 1);
        }
    }

    private static void getAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            Integer num = m.get(Integer.toString(i));
        }
    }

    // Lookup a number of strings in the given map.
    private static void lookupAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = m.containsKey(Integer.toString(i));
        }
    }

    private static void mixAll(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            Integer num = m.remove(Integer.toString(i));
            m.put(Integer.toString(i), num);
        }
    }


    // Now the benchmarks we actually want to run.
    @Bench
    public static void insertOrderedJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            b.start();
            insertOrdered(m);
        }
    }

    @Bench
    public static void insertRandomJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            b.start();
            insertRandom(m);
        }
    }

    @Bench
    public static void removeOrderedJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            removeAll(m);
        }
    }

    @Bench
    public static void removeRandomJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            removeAll(m);
        }
    }

    @Bench
    public static void putOrderedJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            putAll(m);
        }
    }

    @Bench
    public static void putRandomJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            putAll(m);
        }
    }

    @Bench
    public static void getOrderedJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            getAll(m);
        }
    }

    @Bench
    public static void getRandomJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            getAll(m);
        }
    }

    @Bench
    public static void lookupOrderedJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            lookupAll(m);
        }
    }

    @Bench
    public static void lookupRandomJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            lookupAll(m);
        }
    }

    @Bench
    public static void mixedOrderedJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertOrdered(m);
            b.start();
            mixAll(m);
        }
    }

    @Bench
    public static void mixedRandomJavaMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            setupLists();
            insertRandom(m);
            b.start();
            mixAll(m);
        }
    }
}
